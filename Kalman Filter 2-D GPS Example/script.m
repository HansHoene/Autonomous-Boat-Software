%{
Hans-Edward Hoene
25/10/2018

In this Kalman Filter, GPS is used to estimate the two-dimensional position of a
boat.  The boat shall be exerting a force, which shall be modelled as an extered
acceleration in order to avoid assuming the boat's mass.  GPS only measures
position as a longitude and latitude.  GPS does not measure velocity (in this
example).  Therefore, in order to estimate velocity, we shall need to extend the
applicability of a Kalman Filter.

To estimate position and velocity, I shall include an accelerometer to estimate
acceleration.  By measuring acceleration, I can predict the next velocity and
the next position.  To measure position, I shall simply use GPS.  To measure
velocity, however, I shall use the difference between the previously measured
position and the current measured position.

One doubt I have is with the accelerometer.  If the accelerometer is just
another sensor, do I need another Kalman Filter to account to the
accelerometer's noise?  If so, I believe that I can estimate the next
acceleration by measuring the power to the motor.  Then, I can use the Kalman
Filter and the reading from the accelerometer to even that out.  Another doubt I
have concerns the reading from the accelerometer.  If I rotate, will the
accelerometer notice the change in direction?  In this example, I will assume
the accelerometer outputs an accurate 2-D acceleration, with no noise, but in
practical use, I may need to include a compass with some vector calculations,
and I may need a Kalman Filter for this sensor.

deltaT = difference in time from previous iteration (shall be a constant)

X = | x  |
    | x' |
    | y  |
    | y' |    % where x' is change in x, or velocity

A = |      1  deltaT       0       0 |
    |      0       1       0       0 |
    |      0       0       1  deltaT |
    |      0       0       0       1 |

U = external force = | x'' |
                     | y'' | % measured from accelerometer

B = | (deltaT ^ 2) / 2                   0 |
    |           deltaT                   0 |
    |                0    (deltaT ^ 2) / 2 |
    |                0              deltaT |

H = | 1 0 0 0 |
    | 0 1 0 0 |
    | 0 0 1 0 |
    | 0 0 0 1 |

Q = multiple of I (identity matrix, or 1)

R = multiple of | 1 0 0 0 |
                | 0 2 0 0 |
                | 0 0 1 0 |
                | 0 0 0 2 |
The variance of the velocities is twice the variances of the positions.  That is
because velocity is measured as the difference between positions.  The sum of
two normal Gaussian distributions yields approximately another Gaussian
distribution that has a variance equal to the sum of the other two variances
("https://en.wikipedia.org/wiki/Sum_of_normally_distributed_random_variables").

NOTE: To get the Kalman Gain calculation to not result in a singular matrix
error (because the matrix was not invertible), I had to change R to be a square
matrix.  By making R a multiple of the identity matrix, R became automatically
invertible.  Because R is invertible, another matrix plus R is also invertible
(according to "https://dsp.stackexchange.com/questions/33837/why-is-this-matrix-invertible-in-the-kalman-gain").

%}

% ----------------------------- C o n s t a n t s ------------------------------
deltaT           = 1;
I = [1, 0, 0, 0; 0, 1, 0, 0; 0, 0, 1, 0; 0, 0, 0, 1];

Q                = 5 * I;         % covariance of process noise
R                = 10 * ...       % covariance of measurment noise
                  [     1,  2 / 3,      0,      0; ...
                    2 / 3,      2,      0,      0; ...
                        0,      0,      1,  2 / 3; ...
                        0,      0,  2 / 3,     2];

% ------------------------- A c t u a l    V a l u e s -------------------------
realX = [1; 1; 1; 1];
U = [0; 0];

% ---------------------- S i m u l a t i o n    M o d e l ----------------------
estX = [1; 1; 1; 1];
P = [1, 1, 1, 1; 1, 1, 1, 1; 1, 1, 1, 1; 1, 1, 1, 1];
A = [     1, deltaT,      0,      0; ...
          0,      1,      0,      0; ...
          0,      0,      1, deltaT; ...
          0,      0,      0,      1  ...
    ];

B = [(deltaT * deltaT) / 2,                     0; ...
                    deltaT,                     0; ...
                         0, (deltaT * deltaT) / 2; ...
                         0,                deltaT  ...
    ];

H = [1, 0, 0, 0; 0, 1, 0, 0; 0, 0, 1, 0; 0, 0, 0, 1];

Z = [0; 0; 0; 0];   % define a simulated measurment
prevZ = Z;
K = 0;              % define a Kalman Gain

% ------------------------ R u n    S i m u l a t i o n ------------------------

N = 10000;
data = zeros(10, N);

disp(['Running Simulation ', num2str(N), ' times']);

for i = 1:1:N

    % "measure" acceleration from accelerometer
    if (floor(rand * 10) == 0)
        % randomly change acceleration every once in a while
        U = [normrnd(0, 2); normrnd(0, 2)];
    end
    
    % update actual values and simulate a measurement
    realX = (A * realX) + (B * U);
    Z = (H * realX) + ...
        [ normrnd(0, sqrt(R(1, 1))); ...
          normrnd(0, sqrt(R(1, 1))); ...
          normrnd(0, sqrt(R(1, 1))); ...
          normrnd(0, sqrt(R(1, 1)))      ];
    if i == 0
        % for first iteration, measure velocity as zero
        Z(2) = 0;
        Z(4) = 0;
    else
        % after, measure velocity as difference from previous position
        Z(2) = (Z(1) - prevZ(1)) / deltaT;
        Z(4) = (Z(3) - prevZ(3)) / deltaT;
    end
    prevZ = Z;

    % time update for estimate
    estX = (A * estX) + (B * U);
    P = (A * P * transpose(A)) + Q;

    % measurment update for estimate
    K = (P * transpose(H)) / ((H * P * transpose(H)) + R);
    estX = estX + (K * (Z - (H * estX)));
    P = (I - (K * H)) * P;

    % store values for plotting
    data(1, i) = realX(1);
    data(2, i) = realX(3);  % store actual coordinates
    data(3, i) = Z(1);
    data(4, i) = Z(3);      % store measured coordinates
    data(5, i) = estX(1);
    data(6, i) = estX(3);   % store estimated coordinates
    data(7, i) = ((Z(1) - realX(1)) ^ 2) + ((Z(3) - realX(3)) ^ 2);
    data(8, i) = ((estX(1) - realX(1)) ^ 2) + ((estX(3) - realX(3)) ^ 2);
    data(9, i) = mean(data(7, 1:1:i)); % MSE of measurement
    data(10, i) = mean(data(8, 1:1:i)); % MSE of estimate
end

% ----------------------- P r o c e s s    R e s u l t s -----------------------

% determine graph scales
if N < 100
    scale = 1;
else
    scale = floor(N / 100);
end
scale = 1:scale:N;

% print mean squarred errors
disp('Mean Squared Measurement Error = ');
display(data(9, N));
disp('');
disp('Mean Squared Estimate Error = ');
display(data(10, N));

% create a plot of errors
figure(1);
errorPlot = plot( ...
    scale,  data(7, scale), 'r-', ...  % plot measurement error
    scale,  data(8, scale), 'b-', ...  % plot estimate error
    scale,  data(9, scale), 'g--', ...  % MSE measurment
    scale, data(10, scale), 'k--'  ...  % MSE estimate
);
title('Kalman Filter: Measurement Error vs. Estimate Error');
legend('Measurement Error', 'Estimate Error', ...
       'Mean Squarred Error of Measurement', 'Mean Squarred Error of Estimate');
xlabel('time (seconds)');
ylabel('Error (metres squared)');

% create a plot comparing actual position to estimated position
figure(2);
positionPlot = plot( ...
    data(1, scale), data(2, scale), 'k--', ...  % plot actual values
    data(3, scale), data(4, scale), 'r-x', ...  % plot measured values
    data(5, scale), data(6, scale), 'b-o'  ...  % plot estimated values
);
title('Kalman Filter: 2-D Position Simulation');
legend('Actual', 'Measured', 'Estimated');
xlabel('X Position (metres)');
ylabel('Y Position (metres)');

return;
