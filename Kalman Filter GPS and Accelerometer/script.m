%{
Hans-Edward Hoene
25/10/2018

In this file, there are two Kalman Filters.

Imagine a boat.  The boat is rectangular.  There is 

First, the acceleration to the motors is somehow known.
First, the acceleration is estimated based on the power going to the motor.
Then, the estimate is updated based on a measurement from the accelerometer.

The second Kalman Filter is similar to the script in "Kalman Filter 2-D GPS
Example".  The updated estimate of the acceleration is assumed to be accurate.
The acceleration is used to estimate two-dimensional position and velocity.  The
estimates of position and velocity are updated by GPS measurements.  The
position measurements come directly from the GPS measurement.  The velocity
measurements are calculated by finding the difference between the last position
measurement and the current position measurement.  The variance of the velocity
measurement is the variance of the difference between the normal distributions,
which is simply the sum of their variances.

---------------------- K A L M A N    F I L T E R    # 1 -----------------------



---------------------- K A L M A N    F I L T E R    # 2 -----------------------

%}