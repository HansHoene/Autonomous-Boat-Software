% Hans-Edward Hoene
% 24/10/2018
%
% ------------------------- K a l m a n    F i l t e r -------------------------
%
% The Kalman Filter is similar to an exponential rolling average, but the Kalman
% Filter accounts for variance and noise.  An exponential rolling average is as
% follows: nextValue = (alpha * prevValue) + ((1 - alpha) * measValue), where
% alpha is a constant between zero and one.  In a Kalman Filter, alpha is
% referred to as the Kalman Gain, which is updated in every iteration.
%
% A Kalman Filter is a linear recursive estimator.  A Kalman Filter has two
% phases: a time update and a measurement update [1].  You start with a vector of
% values, X, to estimate.  For example, you may have a matrix of values such as
% position and velocity, which is the derivative of position.  Next, you need a
% covariance matrix, P.  X is a vertical vector matrix, while P is a square
% matrix.  P shall contain the uncertainty of the estimate.
%
% For the time update
% phase, you use physical models to update your estimate of X.  The general
% equation is as follows: X <- AX + BU.  A is your linear square matrix
% multiplier.  For example, position would become the previous position plus the
% product of the velocity and the change in time.  U is an external force, such
% as the power flowing to the motor of the car.  By multiplying B by U, you can
% update the overall estimate to account for acceleration.acceleration.  Next,
% for the time update, you need to update your uncertainty, which is done in the
% following equation: P <- APA' + Q, where A' is the transposed matrix of A, and
% Q is the covariance of the process noise [2].
%
% For the measurement update, we use actual measurements to update the Kalman
% gain and to make a better estimate.  The Kalman gain, K, is calculated using
% the following equation: K = PH' * inverse(HPH' + R), or K = PH' / (HPH' + R).
% R is the covariance of the measurement noise.  This is usually simple to
% calculate because most sensors have information about what type of nose they
% are susceptible to and to what degree.  H is the relationship between X and
% whatever is being measured.  For example, if X holds position and velocity,
% and you are measuring position and velocity, then H is equal to the identity
% matrix.  After the Kalman gain is calculated, X is updated according to the
% following equation: X <- X + K(Z - HX), where Z are the measured values.  Note
% that Z - HX is the difference between the actual measurement and the estimated
% measurement.  Afterwards, P is updated according to the following equation:
% P <- (I - KH)P, where I is the identity matrix.
%
% [1] http://bilgin.esme.org/BitsAndBytes/KalmanFilterforDummies
% [2] https://math.stackexchange.com/questions/840662/an-explanation-of-the-kalman-filter
%
% -------------------- S i m u l a t i o n    E x a m p l e --------------------
%
% This file contains a one-dimensional example of a Kalman Filter.  The
% simulator shall track the actual values.  The Kalman Filter shall be
% constantly estimating values and updating its parameters based on the
% measurements it receives.  The measurements shall be equal to the actual
% values plus additional random numbers to simulate the effects of noise.
%
% X shall represent the position and velocity of a vehicle, which travels
% one-dimensionally.  An external force shall be the pedal of the vehicle.  The
% pedal shall be linearly related to the acceleration.

% ----------------------------- C o n s t a n t s ------------------------------
deltaT           = 1;
accelPerPedal    = 2;                        % acceleration per unit pedal
Q                = 5  * [1, 0; 0, 1];        % covariance of process noise
R                = 10 * [1, 0; 0, 1];        % covariance of measurment noise

% ------------------------- A c t u a l    V a l u e s -------------------------
realX            = [1; 1];    % initialise X
U                = [0; 0];    % let pedal only affect bottom part of matrix

% ---------------------- S i m u l a t i o n    M o d e l ----------------------
estX             = [0; 0];
P                = [1, 1; 1, 1];

A                = [1, deltaT; 0, 1];
B                = accelPerPedal * [0, (deltaT * deltaT) / 2; 0, deltaT];

H                = [1, 0; 0, 1];
I                = [1, 0; 0, 1];

Z                = [0; 0];   % define a simulated measurment
K                = 0;        % define a Kalman Gain

% ------------------------ R u n    S i m u l a t i o n ------------------------

N = 10000;
data = zeros(6, N);

disp(['Running Simulation ', num2str(N), ' times']);
for i = 1:N

    % step on the pedal for some iterations
    if (0 <= rem(i, floor(N / 5)) && rem(i, floor(N / 5)) < floor(floor(N / 5) / 10))
        U = [0; floor(i / 10)];
    else
        U = [0; 0];
    end

    % update actual values and simulate a measurement
    realX = (A * realX) + (B * U);
    Z = realX + [normrnd(0, sqrt(R(1, 1))); normrnd(0, sqrt(R(2, 2)))];

    % time update for estimate
    estX = (A * estX) + (B * U);
    P = (A * P * transpose(A)) + Q;

    % measurment update for estimate
    K = (P * transpose(H)) / ((H * P * transpose(H)) + R);
    estX = estX + (K * (Z - (H * estX)));
    P = (I - (K * H)) * P;

    % store error
    data(1, i) = realX(1);
    data(2, i) = Z(1);
    data(3, i) = estX(1);
    data(4, i) = (Z(1) - realX(1)) ^ 2;
    data(5, i) = (estX(1) - realX(1)) ^ 2;
    data(6, i) = mean(data(5, 1:1:i));
end

% ----------------------- P r o c e s s    R e s u l t s -----------------------

% determine graph scales
if N < 100
    scale = 1;
else
    scale = floor(N / 100);
end
scale = 1:scale:N;

% print mean squarred errors
disp('Mean Squared Measurement Error = ');
display(mean(data(4, 1:1:N)));
disp('');
disp('Mean Squared Estimate Error = ');
display(mean(data(5, 1:1:N)));

% create a plot of errors
figure(1);
errorPlot = plot( ...
    scale, data(4, scale), 'r-', ...  % plot measurement error
    scale, data(5, scale), 'b-', ...  % plot estimate error
    scale, data(6, scale), 'k-'  ...  % mean squarred estimate error over time
);
title('Kalman Filter: Measurement Error vs. Estimate Error');
legend('Measurement Error', 'Estimate Error', 'Mean Squarred Error of Estimate');
xlabel('time (seconds)');
ylabel('Error (metres squared)');
set(errorPlot(3), 'linewidth', 2);

% create a plot comparing actual position to estimated position
figure(2);
positionPlot = plot( ...
    scale, data(1, scale), 'k--', ...  % plot actual values
    scale, data(2, scale), 'r-x', ...  % plot measured values
    scale, data(3, scale), 'g-o'  ...  % plot estimated values
);
title('Kalman Filter: 1-D Position Simulation');
legend('Actual', 'Measured', 'Estimated');
xlabel('Time (seconds)');
ylabel('Position (metres)');

return;
