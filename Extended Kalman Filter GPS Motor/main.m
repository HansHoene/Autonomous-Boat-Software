%{
Hans-Edward Hoene
27/11/2018

This script runs an Extended Kalman Filter.  The Kalman Filter uses the
force to the motors to predict the next state.  GPS measurements and a
compass are used to update the state.

State = transpose(x y theta u v r)
Measurements = transpose(x y theta x' y' r)

%}

% --------------------------- C o n s t a n t s ---------------------------
compassVariance = pi/16;  % variance of compass measurements, which return an angle in radians
gpsXVariance    = 2;
gpsYVariance    = 2;

N = 1000; % # of iterations
DELTA_T = 1;

% identity matrix
I = [ ...
    1, 0, 0, 0, 0, 0; ...
    0, 1, 0, 0, 0, 0; ...
    0, 0, 1, 0, 0, 0; ...
    0, 0, 0, 1, 0, 0; ...
    0, 0, 0, 0, 1, 0; ...
    0, 0, 0, 0, 0, 1  ...
];

H = I;
%H(4, 4) = 0;
%H(5, 5) = 0;
%H(6, 6) = 0;

% covariance of process noise
Q = 10 * I;

% covariance of measurement noise
R = zeros(6, 6);
R(1, 1) = gpsXVariance;
R(2, 2) = gpsYVariance;
R(3, 3) = compassVariance;

R(4, 4) = gpsXVariance + gpsYVariance;
%R(1, 4) = 2 * gpsXVariance;
%R(4, 1) = R(1, 4);
%R(2, 4) = 2 * gpsYVariance;
%R(4, 2) = R(2, 4);

R(5, 5) = gpsXVariance + gpsYVariance;
%R(1, 5) = gpsXVariance;
%R(5, 1) = R(1, 5);
%R(2, 5) = gpsYVariance;
%R(5, 1) = R(2, 5);

R(6, 6) = 2 * compassVariance;
%R(3, 6) = 2 * compassVariance;
%R(6, 3) = R(3, 6);

% --------------------------- A l g o r i t h m ---------------------------

% allocate data
data = zeros(8, N);

% initialise state and covariance
P = 100 * [1, 1, 1, 1, 1, 1; 1, 1, 1, 1, 1, 1; 1, 1, 1, 1, 1, 1; 1, 1, 1, 1, 1, 1; 1, 1, 1, 1, 1, 1; 1, 1, 1, 1, 1, 1];
X = [1; 1; 1; 1; 1; 1];
prevX = X;

A = I; % A
actX = [0; 0; 0; 0; 0; 0]; % actual state
K = I;
Z = [0; 0; 0; 0; 0; 0];

% motor forces
LF = 0;
RF = 0;

for i = 1:N
    % set forces
    q = floor(N / 6);
    switch (floor(i / q))
        case 0
            % forward
            LF = 8;
            RF = 8;
        case 1
            % left
            LF = 4;
            RF = 8;
        case 2
            % hard left
            LF = -1;
            RF = 5;
        case 3
            % right
            LF = 4;
            RF = 8;
        case 4
            % hard right
            LF = 5;
            RF = -1;
        case 5
            % backward
            LF = -2;
            RF = -2;
    end
    
    % predict
    prevX = X;
    [X, A] = CalculateNextState(prevX, LF, RF, DELTA_T);
    P = (A * P * transpose(A)) + Q;
    
    % get actual next state
    [actX, phonyA] = CalculateNextState(actX, LF, RF, DELTA_T);
    
    % simulate a measurement
    Z(1) = normrnd(actX(1), sqrt(gpsXVariance));
    Z(2) = normrnd(actX(2), sqrt(gpsYVariance));
    Z(3) = normrnd(actX(3), sqrt(compassVariance));
    Z(4) = (((Z(1) - prevX(1)) * cos(X(3))) + ((Z(2) - prevX(2)) * sin(X(3)))) / DELTA_T;
    Z(5) = ((-(Z(1) - prevX(1)) * sin(X(3))) + ((Z(2) - prevX(2)) * cos(X(3)))) / DELTA_T;
    Z(6) = wrapToPi(Z(4) - prevX(4)) / DELTA_T;
    
    % update
    K = (P * transpose(H)) / ((H * P * transpose(H)) + R);
    X = X + (K * (Z - (H * X)));
    P = (I - (K * H)) * P;
    
    % plot actual X and estimated X
    data(1, i) = actX(1);
    data(2, i) = actX(2);
    data(3, i) = X(1);
    data(4, i) = X(2);
    data(5, i) = Z(1);
    data(6, i) = Z(2);
    data(7, i) = sqrt( ...
                       ((X(1) - actX(1)) * (X(1) - actX(1)))   ...
                       + ((X(2) - actX(2)) * (X(2) - actX(2))) ...
                      );
    data(8, i) = sqrt( ...
                        ((Z(1) - actX(1)) * (Z(1) - actX(1)))   ...
                        + ((Z(2) - actX(2)) * (Z(2) - actX(2))) ...
                      );
    
    disp([num2str(i), ': Meas Err = ', num2str(data(8, i)), ', Est Err = ', num2str(data(7, i))]);;
    %disp(['i = ', num2str(i), ', Measured Error = ', num2str(data(8, i)), ', Estimate Error = ', num2str(data(7, i))]);

    ++i;
end

if N > 100
    scale = floor(N / 100);
else
    scale = 1;
end
scale = 1:scale:N;

% plot positions
figure(1);
plot1 = plot( ...
              data(1, scale), data(2, scale), 'k--', ...
              data(3, scale), data(4, scale), 'r-x', ...
              data(5, scale), data(6, scale), 'b-o'  ...
              );
title('Position Plot');
legend('Actual', 'Estimated', 'Measured');
xlabel('X position (metres)');
ylabel('Y position (metres)');

% plot errors
figure(2);
plot2 = plot( ...
        scale, data(7, scale), 'k-', ...
        scale, data(8, scale), 'r-' ...
        );
title('Error Plot');
legend('Estimate Error', 'Measurement Error');
xlabel('Time (seconds)');
ylabel('Error (metres)');
