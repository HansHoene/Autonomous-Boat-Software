function [nextState, A] = CalculateNextState(curState, F1, F2, deltaT)

%{
State = [x; y; theta; u; v; r];
         1  2  3  4      5      6

Output:
    nextState - the next predicted state

Input:
    curState - the current state
    F2 - right motor's force
    F1 - left motor's force
    deltaT - the change in time
%}

% --------------------------- C o n s t a n t s ---------------------------

    Fmax = 50;  % maximum motor force
    m    = 100; % mass of boat
    Iz   = 20;  % kilogram meter squared - interia
    mx   = 10;   % kilograms - added mass in x-direction from water resistance
    my   = 10;   % kilograms - added mass from water resistance
    Jz   = 2;   % kilogram meter squared - added inertia from water resistance
    s    = 0.5;   % metres - distance between G and jets
    alpha0 = pi / 4; % angle defined by arctan((half dist between starboard and port jets) / (distance from G to astern))
    
    % A(1, 1) to A(1, 3) = hydrodynamic coefficients in surge motion
    % A(2, 1) to A(2, 3) = hydrodynamic coefficients in sway motion
    % A(3, 1) to A(3, 3) = hydrodynamic coefficients in yaw motion
    Ab   = 5 * [1, 1, 1; 1, 1, 1; 1, 1, 1];
    
% Physical Models
syms x y theta u v r;
up = ( ...
         (m * r * v) + F1 + F2 - (u * Ab(1, 1)) ...
         - (u * abs(u) * Ab(1, 2)) - (u * u * u * Ab(1, 3)) ...
     ) / (m + mx);
vp = - ( ...
           (m * r * u) + (v * Ab(2, 1)) + (v * abs(v) * Ab(2, 2)) ...
           + (v * v * v * Ab(2, 3)) ...
       ) / (m + my);
rp = ( ...
         ((F2 - F1) * sin(alpha0)) - (r * Ab(3, 1)) ...
         - (r * abs(r) * Ab(3, 2)) ...
         - (r * r * r * Ab(3, 3)) ...
     ) / (Iz + Jz);
nextU = u + (deltaT * up);
nextV = v + (deltaT * vp);
nextR = r + (deltaT * rp);
nextTheta = theta + (deltaT * r) + ((deltaT / 2) * rp);
nextX = x + ...
        (deltaT * (...
            ((u * cos(theta)) - (v * sin(theta))) ...
        )) + (deltaT * (deltaT / 2) * ( ...
            (up * cos(theta)) - (vp * sin(theta)) - (r * ( ...
                (u * sin(theta)) + (v * cos(theta)) ... 
             )) ...
        ));
nextY = y + (deltaT * ( ...
            (u * sin(theta)) + (v * cos(theta)) ...
        )) + (deltaT * (deltaT / 2) * ( ...
            (up * sin(theta)) + (vp * cos(theta)) + (r * ( ...
                (u * cos(theta)) - (v * sin(theta)) ...
             )) ...
        ));

% get A
vars = [x; y; theta; u; v; r];
outs = [nextX; nextY; nextTheta; nextU; nextV; nextR];
A = zeros(6, 6);
for i = 1:6
    for j = 1:6
        A(i, j)= eval(subs(diff(outs(i), vars(j)), vars, curState));
    end
end

% get next state
nextState = zeros(6, 1);
for i=1:6
    nextState(i, 1) = eval(subs(outs(i), vars, curState));
end

nextState(3, 1) = wrapTo2Pi(nextState(3, 1));

end
