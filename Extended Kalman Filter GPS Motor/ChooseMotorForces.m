function [lf, rf] = ChooseMotorForces(X, dest)

%{
    This function chooses the motor forces accordingly in order to reach the
    destination.

    Inputs:
        X - state vector of length six (x, y, theta, x', y', theta')
        dest - destination vector of length 2 (x, y)

    Outputs:
        lf - left motor force
        rf - right motor force

%}

    % Constants
    MAX_MOTOR_FORCE = 50;
    MAX_DIST = 0.1; % maximum allowable distance from dest
    
    MIN_FORWARD_FORCE = 0.1;
    MAX_FORWARD_FORCE = 10;
    MIN_BACKWARD_FORCE = 0.1;
    MAX_BACKWARD_FORCE = 4;
    FORCE_STRETCH = 25;
    
    MIN_TOLERATED_ANGLE = pi / 100;
    MAX_TOLERATED_ANGLE = pi / 6;
    TOLERATED_ANGLE_STRETCH = 25;
    
    FB =  1; % forward boolean
    BB = -1; % backward boolean
    OB  = 0; % off boolean
    
    % get distance from destination
    dx = dest(1) - X(1);
    dy = dest(2) - X(2);
    dTotal = sqrt((dx * dx) + (dy * dy));
    
    % close enough?
    if (dTotal <= MAX_DIST)
        lf = 0;
        rf = 0;
        return;
    end
    
    % get desired angle
    if (dx == 0)
        if (dy >= 0)
            angle = pi / 2;
        else
            angle = -pi / 2;
        end
    else
        angle = atan(dy / dx);
        if (dx >= 0)
            if (dy >= 0)
                % fine
            else
                % wrap to 2 PI
                angle = angle + (2 * pi);
            end
        else
            angle = angle + pi;
        end
    end
    
    % tolerated angle error
    toleratedAngleError = MIN_TOLERATED_ANGLE ...
                        + ((MAX_TOLERATED_ANGLE - MIN_TOLERATED_ANGLE) * ...
                           (1 - exp(-dTotal / TOLERATED_ANGLE_STRETCH)) ...
                           );
    
    % turn or go forward?
    deltaAng = wrapToPi(angle - X(3));
    if (abs(deltaAng) > toleratedAngleError)
        % turn
        
        % pretend to turn left
        lf = - MIN_BACKWARD_FORCE - ( ...
                (MAX_BACKWARD_FORCE - MIN_BACKWARD_FORCE) * ( ...
                     1 ...
                     - (exp(-abs(deltaAng) / TOLERATED_ANGLE_STRETCH) / 2) ...
                     - (exp(-abs(dTotal) / FORCE_STRETCH) / 2) ...
             ));
        rf = MIN_FORWARD_FORCE + ( ...
                (MAX_FORWARD_FORCE - MIN_FORWARD_FORCE) * ( ...
                     1 ...
                     - (exp(-abs(deltaAng) / TOLERATED_ANGLE_STRETCH) / 2) ...
                     - (exp(-abs(dTotal) / FORCE_STRETCH) / 2) ...
             ));
         
         if (deltaAng < 0)
            % actually, go right
            x = lf;
            lf = rf;
            rf = x;
         end
    else
        % go forward
        lf = MIN_FORWARD_FORCE + ( ...
                (MAX_FORWARD_FORCE - MIN_FORWARD_FORCE) * ( ...
                     1 - exp(-abs(dTotal) / FORCE_STRETCH) ...
             ));
         rf = MIN_FORWARD_FORCE + ( ...
                (MAX_FORWARD_FORCE - MIN_FORWARD_FORCE) * ( ...
                     1 - exp(-abs(dTotal) / FORCE_STRETCH) ...
             ));
    end
    
end
