%{
Hans-Edward Hoene
27/11/2018

This script runs an Extended Kalman Filter.  The Kalman Filter uses the
force to the motors to predict the next state.  GPS measurements and a
compass are used to update the state.

State = transpose(x y theta u v r)
Measurements = transpose(x y theta x' y' r)

%}

% --------------------------- C o n s t a n t s ---------------------------
compassVariance = pi / 16;  % variance of compass measurements, which return an angle in radians
gpsXVariance    = 2;
gpsYVariance    = 2;

N = 5000; % max # of iterations
DELTA_T = 1;

start = [0, 0, pi * (1 / 3)]; % starting (X, Y, theta) location
dest  = [ ...
         25, 25; ...
         0, 100; ...
         -25, 25; ...
         0, 0; ...
         25, -25; ...
         0, -100; ...
         -25, -25; ...
         0, 0; ...
         ];  % intended (X, Y) destination
numDest = 8;

% identity matrix
I = [ ...
    1, 0, 0, 0, 0, 0; ...
    0, 1, 0, 0, 0, 0; ...
    0, 0, 1, 0, 0, 0; ...
    0, 0, 0, 1, 0, 0; ...
    0, 0, 0, 0, 1, 0; ...
    0, 0, 0, 0, 0, 1  ...
];

H = I;
%H(4, 4) = 0;
%H(5, 5) = 0;
%H(6, 6) = 0;

% covariance of process noise
Q = 0.25 * I;

% covariance of measurement noise
R = zeros(6, 6);
R(1, 1) = gpsXVariance;
R(2, 2) = gpsYVariance;
R(3, 3) = compassVariance;

R(4, 4) = gpsXVariance + gpsYVariance;
%R(1, 4) = 2 * gpsXVariance;
%R(4, 1) = R(1, 4);
%R(2, 4) = 2 * gpsYVariance;
%R(4, 2) = R(2, 4);

R(5, 5) = gpsXVariance + gpsYVariance;
%R(1, 5) = gpsXVariance;
%R(5, 1) = R(1, 5);
%R(2, 5) = gpsYVariance;
%R(5, 1) = R(2, 5);

R(6, 6) = 2 * compassVariance;
%R(3, 6) = 2 * compassVariance;
%R(6, 3) = R(3, 6);

% --------------------------- A l g o r i t h m ---------------------------

% allocate data
data = zeros(N, 6 + 6 + 6 + 2); % store actX, Z, X, F1 and F2
csvFileOut = uiputfile('*.csv', 'Create Log File');
figFileOut = uiputfile('*.png', 'Create Figure');

% initialise state and covariance
P = 100 * [1, 1, 1, 1, 1, 1; 1, 1, 1, 1, 1, 1; 1, 1, 1, 1, 1, 1; 1, 1, 1, 1, 1, 1; 1, 1, 1, 1, 1, 1; 1, 1, 1, 1, 1, 1];
X = [1; 1; 1; 1; 1; 1];
prevX = X;

A = I; % A
actX = [start(1); start(2); start(3); 0; 0; 0]; % actual state
K = I;
Z = [0; 0; 0; 0; 0; 0];

% motor forces
LF = 0;
RF = 0;

destIndex = 1;

counter = 0;
while (counter < N && destIndex <= numDest)
    counter = counter + 1;
    
    % set forces
    [LF, RF] = ChooseMotorForces(X, [dest(destIndex, 1), dest(destIndex, 2)]);
    
    % predict
    prevX = X;
    [X, A] = CalculateNextState(prevX, LF, RF, DELTA_T);
    P = (A * P * transpose(A)) + Q;
    
    % get actual next state
    [actX, phonyA] = CalculateNextState(actX, LF, RF, DELTA_T);
    
    % simulate a measurement
    Z(1) = normrnd(actX(1), sqrt(gpsXVariance));
    Z(2) = normrnd(actX(2), sqrt(gpsYVariance));
    Z(3) = normrnd(actX(3), sqrt(compassVariance));
    Z(4) = (((Z(1) - prevX(1)) * cos(X(3))) + ((Z(2) - prevX(2)) * sin(X(3)))) / DELTA_T;
    Z(5) = ((-(Z(1) - prevX(1)) * sin(X(3))) + ((Z(2) - prevX(2)) * cos(X(3)))) / DELTA_T;
    Z(6) = wrapToPi(Z(4) - prevX(4)) / DELTA_T;
    
    % update
    K = (P * transpose(H)) / ((H * P * transpose(H)) + R);
    X = X + (K * (Z - (H * X)));
    P = (I - (K * H)) * P;
    
    % plot actual X and estimated X
    data(counter, 1:6) = actX;
    data(counter, 7:12) = Z;
    data(counter, 13:18) = X;
    data(counter, 19) = LF;
    data(counter, 20) = RF;
    
    % how far from next dest?
    temp = sqrt( ...
               ((dest(destIndex, 1) - X(1)) * (dest(destIndex, 1) - X(1))) + ...
               ((dest(destIndex, 2) - X(2)) * (dest(destIndex, 2) - X(2))) ...
    );
    
    disp([ num2str(counter), ': ', ...
                      'dX = ', num2str(dest(destIndex, 1) - X(1)), ', ', ...
                      'dY = ', num2str(dest(destIndex, 2) - X(2)), ', ', ...
                   'Theta = ',                      num2str(X(3)), ' | ', ...
              'Dest Index = ',                 num2str(destIndex), ', ', ...
          'Dist from Dest = ',                      num2str(temp)        ...
          ]);
      
    % did I reach next dest?
    if (temp < 2)
        destIndex = destIndex + 1;
    end
end

% export everything to CSV
if csvFileOut ~= 0
    csvwrite(csvFileOut, data);
    disp('');
    disp(['Data Exported to CSV: ', csvFileOut]);
    disp('CSV Headers:');
    disp('   1: 6 = Actual State');
    disp('   7:12 = Measured State');
    disp('  13:18 = Estimated State');
    disp('  19:20 = Left/Right Motor');
end

scale = 1:counter;

% --------------------- P l o t    P o s i t i o n s ----------------------

figure('Position', get(0, 'Screensize'));
p1 = plot( ...
              data(scale,  1),      data(scale,  2), 'k--', ... % plot actual (x, y)
              data(scale,  7),      data(scale,  8),  'co', ... % plot measured (X, Y)
              data(scale, 13),      data(scale, 14),  'bo', ... % plot estimated (X, Y)
                     start(1),             start(2),  'gx', ... % plot starting point
         dest(1:1:numDest, 1), dest(1:1:numDest, 2),  'rx'  ... % plot destination points
);

% add labels to starting and destination points
% also, increase point thickness
text(start(1), start(2), 'Start', ...
     'VerticalAlignment', 'bottom', ...
     'HorizontalAlignment', 'left');
text(dest(1:numDest, 1), dest(1:numDest, 2), ...
     cellstr(strcat('Dest #', num2str([1:numDest]'))), ...
     'VerticalAlignment', 'bottom', ...
     'HorizontalAlignment', 'left');
p1(4).LineWidth = 5;
p1(5).LineWidth = 5;

% skip over some marker indices (prevent marker crowding)
if counter > 100
    p1(2).MarkerIndices = 1:(floor(counter / 100)):counter;
    p1(3).MarkerIndices = 1:(floor(counter / 100)):counter;
else
    p1(2).MarkerIndices = 1:1:counter;
    p1(3).MarkerIndices = 1:1:counter;
end

% set title
title('Position Plot');
legend('Actual', 'Measured', 'Estimated');
xlabel('Horizontal Position (metres)');
ylabel('Vertical Position (metres)');

if figFileOut ~= 0
    print(figFileOut, '-dpng');
    disp('');
    disp(['Figure Exported to File: ', figFileOut]);
end

% Calculate Mean-Square Error
disp('');
sum = 0;
for i = 1:1:counter
    sum = sum ...
          + ((data(i, 1) - data(i, 7)) * (data(i, 1) - data(i, 7))) ...
          + ((data(i, 2) - data(i, 8)) * (data(i, 2) - data(i, 8)));
end
sum = sum / counter;
disp(['Mean Squared Measurement Error = ', num2str(sum)]);

sum = 0;
for i = 1:1:counter
    sum = sum ...
          + ((data(i, 1) - data(i, 13)) * (data(i, 1) - data(i, 13))) ...
          + ((data(i, 2) - data(i, 14)) * (data(i, 2) - data(i, 14)));
end
sum = sum / counter;
disp(['Mean Squared Estimate Error = ', num2str(sum)]);

disp('Program Finished');
