%{
    File: "main.m"
    Author: Hans-Edward Hoene

    This is the final result for Multi-Sensor Data Fusion, which is a
    research-based class taught at the University of Massachusetts by Dr.
    Thanuka Wickramarathne.

    This file contains a simulation for an autonomous boat.  Full design
    details and sources are mentioned in the research paper.

    To implement a trash-collecting boat, we shall use my modification of a
    VFF.  When the boat detects a wall in more than one Lidar point, the
    boat will assume that a wall is there.  With more than one data point,
    the boat shall also know the angle of the wall.  The boat will create
    an imaginary vector from the wall that is perpednicular to the wall
    angle plus some bias.  The bias allows that boat to serve between
    section sof the wall.  Without a bias, the boat would probably just go
    back and forth.  The motors will of course turn faster when the angle
    error is greater, but when no walls are detected, the boat will
    have a "vector memory", which allows is to maintain an angle going
    forward.  As soon as a wall is detected in at least one data point, the
    boat shall slow down so that it has time to turn when the next data
    point is detected.

    The Lidar will only point forward.  It will have six data points, which
    means a data points every thirty degrees.

    Modification History
    Date (dd/mm/yyyy)       Description
    ----------------------------------------------------------------------------
    18/12/2018              Created
%}

%{ --------------------------- C o n s t a n t s ---------------------------- %}

% starting location
START_X = 5;
START_Y = 5;
START_THETA = pi;

NUM_LIDAR_POINTS = 7;
LIDAR_RANGE = 10;

DELTA_T = 1;

%{ -------------------------------- M a i n --------------------------------- %}

% simulation localisation variables
actX        = START_X;         % x-position
actY        = START_Y;         % y-position
actTheta    = START_THETA;     % heading angle
actU        = 0;               % surge (forward) velocity
actV        = 0;               % sway (sideways) velocity
actR        = 0;               % yaw (angular) velocity

% simulation obstacle detection variables
% these values must be calculated from the UpdateLidarValues function
actLidar = zeros(1, NUM_LIDAR_POINTS);

% estimated localisation variables
x           = 0;               % x-position
y           = 0;               % y-position
theta       = 0;               % heading angle
u           = 0;               % surge (forward) velocity
v           = 0;               % sway (sideways) velocity
r           = 0;               % yaw (angular) velocity

% estimated obstacle detection variables
lidar = LIDAR_RANGE * ones(1, NUM_LIDAR_POINTS);

% force exerted by left and right motors
Force = [0, 0];
aim = 3 * pi / 2;
state = 3;

N = 1000; % maximum # of iterations
data = zeros(N, 4);

P = 10 * ones(6 + NUM_LIDAR_POINTS, 6 + NUM_LIDAR_POINTS);

i = 1;
while and(i <= N, and(actY > -10, actY < 10))
    % convert states to vectors
    [actState] = StateToVector(actX, actY, actTheta, actU, actV, actR, actLidar, NUM_LIDAR_POINTS);
    [estState] = StateToVector(x, y, theta, u, v, r, lidar, NUM_LIDAR_POINTS);
    
    % update the simulation
    [actState] = UpdateSimulation(actState, Force, DELTA_T, NUM_LIDAR_POINTS, LIDAR_RANGE);
    
    % run Kalman Filter for estimate
    [estState, P, ~] = RunKalmanFilter(estState, actState, P, Force, DELTA_T, NUM_LIDAR_POINTS);
    
    % convert vectors back to states
    [actX, actY, actTheta, actU, actV, actR, actLidar] = VectorToState(actState, NUM_LIDAR_POINTS);
    [x, y, theta, u, v, r, lidar] = VectorToState(estState, NUM_LIDAR_POINTS);
    
    % make a decision
    [Force(1), Force(2), aim, state] = MakeDecision(theta, aim, state, lidar, NUM_LIDAR_POINTS, LIDAR_RANGE);
    
    % store actual vs. estimated
    data(i, 1) = actX;
    data(i, 2) = actY;
    data(i, 3) = x;
    data(i, 4) = y;
    
    % print update
    disp([num2str(i), ': (x, y, theta) = ', '(', num2str(actX), ', ', num2str(actY), ', ', num2str(actTheta), ') Forces = ', num2str(Force(1)), ', ', num2str(Force(2)), ', Lidar = ', strcat(num2str(actLidar(1:NUM_LIDAR_POINTS)), ', ')]);
    i = i + 1;
end
N = i - 1;

% plot
figure('Position', get(0, 'Screensize'));
plot(data(1:N, 1), data(1:N, 2), 'k-', data(1:N, 3), data(1:N, 4), 'b-');
title('Boat Position');
legend('Actual', 'Estimated');
xlabel('X position (metres)');
ylabel('Y position (metres)');
print('results', '-dpng');

%{ --------------------------- F u n c t i o n s ---------------------------- %}
function [angle] = GetLidarAngle(index, curDir, numLdrPts)
    angle = curDir + (pi / 2) - ((pi * (index - 1)) / (numLdrPts - 1));
    angle = wrapTo2Pi(angle);
end

function [LF, RF, aim, state] = MakeDecision(curDir, aim, state, lidar, numLdrPts, ldrRng)
    [aim, state] = DetermineState(aim, state, curDir, lidar, numLdrPts, ldrRng);
    [LF, RF] = Control(aim, curDir, state);
    disp(['State is ', num2str(state)]);
    disp(['Aim is ', num2str(aim)]);
end

function [aim, state] = DetermineState(aim, state, curDir, lidar, numLdrPts, ldrRng)
    bias = pi / 12;
    pts = zeros(numLdrPts, 2);
    mult = 0.5;
    
    % count how many LIDARs detect a wall
    count = 0;
    for i=1:numLdrPts
        if lidar(i) < (mult * ldrRng)
            count = count + 1;
            pts(count, 1) = ((mult * ldrRng) - lidar(i)) * cos(GetLidarAngle(i, curDir, numLdrPts));
            pts(count, 2) = ((mult * ldrRng) - lidar(i)) * sin(GetLidarAngle(i, curDir, numLdrPts));
        end
    end
    
    if (count ~= 0)
        % add all vectors
        for i = 2:count
            pts(1, 1) = pts(1, 1) + pts(i, 1);
            pts(1, 2) = pts(1, 2) + pts(i, 2);
        end
        
        % subtract sum from current vector
        aim = atan(pts(1, 2) / pts(1, 1));
        if (pts(1, 1) >= 0)
            aim = aim + pi;
        end
        aim = wrapTo2Pi(aim);
        if (aim > pi)
            aim = aim + bias;
        else
            aim = aim - bias;
        end
    end
    
    %{
    if count > state
        state = count;
        if state > 1
            % calculate new aim
            slope = polyfit(pts(1:count, 1), pts(1:count, 2), 1);
            slope = slope(1);
            if (slope == 0)
                aim = pi / 2;
            else
                slope = -1 / slope;
                aim = atan(slope);
            end
            if (curDir >= 0)
                aim = -1 * aim;
            end
            aim = wrapTo2Pi(aim);
            if (aim > pi)
                aim = aim + bias;
            else
                aim = aim - bias;
            end
        end
    else
        if count == 0
            state = 0;
        end
    end
    %}
end

function [LF, RF] = Control(aim, curDir, state)
    errorTolerance = pi / 12;
    m = 3; % instead of 1

    angleError = wrapToPi(aim - curDir);
    
    if (state > 0)
        h = 2;
    else
        h = 5;
    end
    
    RF = h * (1 + (m * angleError / pi));
    LF = h * (1 - (m * angleError / pi));
    
    if (RF < 0)
        %RF = 0;
    elseif (RF > (2 * h))
        RF = (2 * h);
    end
    
    if (LF < 0)
        %LF = 0;
    elseif (LF > (2 * h))
        LF = (2 * h);
    end
    
    %{
    LF = h * (exp(-abs(angleError) * 3 / pi));
    RF = h * (1 - exp(-abs(angleError) * 3 / pi));
    
    if angleError >= 0
        t = LF;
        LF = RF;
        RF = t;
    end
    %}
    
    disp(['Angle Error is ', num2str(angleError)]);
    
    %{
    if or(state >= 2, abs(angleError) > (errorTolerance * 3))
        % we are either in state 2, where avoidance is necessary, or we are
        % the angle error is large
        LF = 1 + ((3 - 1) * exp(-abs(angleError) / 100));
        if angleError >= 0
            RF = LF;
            LF = 0;
        else
            RF = 0;
        end
    elseif (abs(angleError) > errorTolerance)
        % we are in state 1 or 2, and there is a relatively minor
        % adjustment to make
        if state < 1
            LF = 3;
        else
            LF = 10;
        end
        
        LF = LF + 1 + ((3 - 1) * exp(-abs(angleError) / 10));
        RF = LF - 1 - ((3 - 1) * exp(-abs(angleError) / 10));
        
        if (angleError >= 0)
            t = LF;
            LF = RF;
            RF = t;
        end
    end
    %}
end

function [estState, P, Z] = RunKalmanFilter(estState, actState, P, Force, dt, numLidarPts)
    % variances
    gpsVar = 1;
    compassVar = pi / 100;
    sensorVar = 0.3;
    
    I = zeros(6 + numLidarPts, 6 + numLidarPts);
    for i = 1:(6 + numLidarPts)
        I(i, i) = 1;
    end

    % measurement noise
    R = zeros(6 + numLidarPts, 6 + numLidarPts);
    R(1, 1) = gpsVar;
    R(2, 2) = gpsVar;
    R(3, 3) = compassVar;
    R(4, 4) = 2 * gpsVar;
    R(5, 5) = 2 * gpsVar;
    R(6, 6) = 2 * compassVar;
    for i = 1:numLidarPts
        R(6 + i, 6 + i) = sensorVar;
    end

    % process noise
    Q = 1 * I;
    
    H = I;

    % prediction
    prevState = estState;
    [estState, A] = CalculateNextState(estState, Force(1), Force(2), dt, numLidarPts);
    P = (A * P * transpose(A)) + Q;
    for i=1:(6 + numLidarPts)
        assert(not(isnan(estState(i))));
    end
    for i=1:(6 + numLidarPts)
        for j=1:(6 + numLidarPts)
            assert(not(isnan(P(i))));
        end
    end
    
    % create phony measurments
    Z = zeros(6 + numLidarPts, 1);
    Z(1) = actState(1) + normrnd(0, sqrt(gpsVar));
    Z(2) = actState(2) + normrnd(0, sqrt(gpsVar));
    Z(3) = actState(3) + normrnd(0, sqrt(compassVar));
    Z(3) = wrapTo2Pi(Z(3));
    dx = (Z(1) - prevState(1)) / dt;
    dy = (Z(2) - prevState(2)) / dt;
    Z(4) = (dx * cos(estState(3))) + (dy * sin(estState(3)));
    Z(5) = (dy * cos(estState(3))) - (dx * sin(estState(3)));
    Z(6) = (Z(3) - prevState(3)) / dt;
    for i=1:numLidarPts
        Z(6 + i) = actState(6 + i) + normrnd(0, sqrt(sensorVar)); 
    end
    for i=1:(6 + numLidarPts)
        assert(not(isnan(Z(i))), 'Measurement #%s failed', num2str(i));
    end
    
    if (Z(3) - estState(3)) > pi
        Z(3) = Z(3) - (2 * pi);
    elseif (estState(3) - Z(3)) > pi
        Z(3) = Z(3) + (2 * pi);
    end
    
    % update
    K = (P * transpose(H)) / ((H * P * transpose(H)) + R);
    for i=1:(6 + numLidarPts)
        for j=1:(6 + numLidarPts)
            assert(not(isnan(K(i))));
        end
    end
    estState = estState + (K * (Z - (H * estState)));
    P = (I - (K * H)) * P;
    for i=1:(6 + numLidarPts)
        for j=1:(6 + numLidarPts)
            assert(not(isnan(P(i))));
        end
    end
    for i=1:(6 + numLidarPts)
        assert(not(isnan(estState(i))));
    end
    
    estState(3) = wrapTo2Pi(estState(3));
end

function [actState] = UpdateSimulation(actState, Force, dt, numLidarPts, lidarRng)
    [actState, ~] = CalculateNextState(actState, Force(1), Force(2), dt, numLidarPts);
    
    % update LIDAR values
    for i=1:numLidarPts
        [actState(6 + i)] = GetDistToWall(actState(1), actState(2), GetLidarAngle(i, actState(3), numLidarPts), lidarRng);
    end
    
    for i=1:(6 + numLidarPts)
        assert(not(isnan(actState(i))), 'Actual State Error (#%s)', num2str(i));
    end
end

function [dist] = GetDistToWall(x, y, theta, lidarRng)
    % lines that represent canal walls
    M = 0;
    B1 = 10;
    B2 = -10;
    
    slope = tan(theta);
    
    if (M == slope)
        dist = lidarRng;
    else
        x1 = (y -(x * slope) - B1) / (M - slope);
        x2 = (y - (x * slope) - B2) / (M - slope);
        y1 = (M * x1) + B1;
        y2 = (M * x2) + B2;
        dx1 = x1 - x;
        dx2 = x2 - x;
        dy1 = y1 - y;
        dy2 = y2 - y;
        
        if (xor(dx1 >= 0, abs(theta) <= (pi / 2)))
            d1 = lidarRng;
        else
            d1 = sqrt((dx1 * dx1) + (dy1 * dy1));
        end
        
        if (xor(dx2 >= 0, abs(theta) <= (pi / 2)))
            d2 = lidarRng;
        else
            d2 = sqrt((dx2 * dx2) + (dy2 * dy2));
        end
        
        if d1 <= d2
            dist = d1;
        else
            dist = d2;
        end
    end
    
    % account for limited range
    if (dist > lidarRng)
        dist = lidarRng;
    end
end

function [state] = StateToVector(x, y, theta, u, v, r, lidar, numLidarPts)
    state = zeros(6 + numLidarPts, 1);
    state(1) = x;
    state(2) = y;
    state(3) = theta;
    state(4) = u;
    state(5) = v;
    state(6) = r;
    for i=1:numLidarPts
        state(6 + i) = lidar(i);
    end
end

function [x, y, theta, u, v, r, lidar] = VectorToState(state, numLidarPts)
    lidar = zeros(1, numLidarPts);
    x = state(1);
    y = state(2);
    theta = state(3);
    u = state(4);
    v = state(5);
    r = state(6);
    for i=1:numLidarPts
        lidar(i) = state(6 + i);
    end
end

function [curState, A] = CalculateNextState(curState, F1, F2, dt, numLidarPts)

    % --------------------------- C o n s t a n t s ---------------------------

    m    = 10; % mass of boat
    Iz   = 10;  % kilogram meter squared - interia
    mx   = 2;   % kilograms - added mass in x-direction from water resistance
    my   = 2;   % kilograms - added mass from water resistance
    Jz   = 2;   % kilogram meter squared - added inertia from water resistance
    alpha0 = pi / 4; % angle defined by arctan((half dist between starboard and port jets) / (distance from G to astern))
    
    % A(1, 1) to A(1, 3) = hydrodynamic coefficients in surge motion
    % A(2, 1) to A(2, 3) = hydrodynamic coefficients in sway motion
    % A(3, 1) to A(3, 3) = hydrodynamic coefficients in yaw motion
    Ab   = 5 * [1, 1, 1; 1, 1, 1; 1, 1, 1];
    
    % Physical Models
    syms x y theta u v r;
    syms(sym('lidar', [1 numLidarPts]));
    
    up = ( ...
         (m * r * v) + F1 + F2 - (u * Ab(1, 1)) ...
         - (u * abs(u) * Ab(1, 2)) - (u * u * u * Ab(1, 3)) ...
     ) / (m + mx);
    vp = - ( ...
           (m * r * u) + (v * Ab(2, 1)) + (v * abs(v) * Ab(2, 2)) ...
           + (v * v * v * Ab(2, 3)) ...
       ) / (m + my);
    rp = ( ...
         ((F2 - F1) * sin(alpha0)) - (r * Ab(3, 1)) ...
         - (r * abs(r) * Ab(3, 2)) ...
         - (r * r * r * Ab(3, 3)) ...
     ) / (Iz + Jz);
    nextU = u + (dt * up);
    nextV = v + (dt * vp);
    nextR = r + (dt * rp);
    nextTheta = theta + (dt * r) + ((dt / 2) * dt * rp);
    nextX = x + ...
        (dt * (...
            ((u * cos(theta)) - (v * sin(theta))) ...
        )) + (dt * (dt / 2) * ( ...
            (up * cos(theta)) - (vp * sin(theta)) - (r * ( ...
                (u * sin(theta)) + (v * cos(theta)) ... 
             )) ...
        ));
    nextY = y + (dt * ( ...
            (u * sin(theta)) + (v * cos(theta)) ...
        )) + (dt * (dt / 2) * ( ...
            (up * sin(theta)) + (vp * cos(theta)) + (r * ( ...
                (u * cos(theta)) - (v * sin(theta)) ...
             )) ...
        ));
    
    % get A and next state
    vars = [x; y; theta; u; v; r; transpose(sym('lidar', [1 numLidarPts]))];
    outs = [nextX; nextY; nextTheta; nextU; nextV; nextR];
    A = zeros(6 + numLidarPts, 6 + numLidarPts);
    nextState = zeros(6 + numLidarPts, 1);
    
    for i = 1:6
        for j = 1:(6 + numLidarPts)
            A(i, j)= eval(subs(diff(outs(i), vars(j)), vars, curState));
        end
        nextState(i, 1) = eval(subs(outs(i), vars, curState));
    end
    
    nextState(3, 1) = wrapTo2Pi(nextState(3, 1));
    for i = 1:numLidarPts
        relAngle = GetLidarAngle(i, 0, numLidarPts);
        nextVal = sym(strcat('lidar', num2str(i))) - (((u * cos(relAngle)) + (v * sin(relAngle))) * dt);
        for j = 1:(6 + numLidarPts)
            A(6 + i, j) = eval(subs(diff(nextVal, vars(j)), vars, curState));
        end
        nextState(6 + i, 1) = eval(subs(nextVal, vars, curState));
    end
    
    for i=1:(6 + numLidarPts)
        if abs(curState(i) - nextState(i)) > 4
            disp('ERROR!');
        end
        curState(i) = nextState(i);
    end
end
